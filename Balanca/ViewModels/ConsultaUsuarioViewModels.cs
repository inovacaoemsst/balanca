﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core.Objects.DataClasses;
using Balanca.Common.Enumerators;

namespace Balanca.ViewModels
{
    public class ConsultaUsuarioViewModels
    {

        public int UsuarioID { get; set; }
        public string Matricula { get; set; }
        public int EmpresaID { get; set; }
        public string CPF { get; set; }
        public string Nome { get; set; }

        public string Setor { get; set; }
        public string Cargo { get; set; }
        public DateTime DataNascimento { get; set; }
        public int Idade { get; set; }
        public string NomeEmpresa { get; set; }
        public int CODIGOEMPRESA { get; set; }
        public string CODIGOUNIDADE { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Sexo:")]
        public Enumerators.SexoEnum Sexo { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Qual sua escolaridade? Maior grau escolar concluído:")]
        public Enumerators.EscolaridadeEnum Escolaridade { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Qual o seu estado civil:")]
        public Enumerators.EstadoCivilEnum EstadoCivil { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você tem filhos ?")]
        public Enumerators.FilhosEnum Filhos { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Há quanto tempo você trabalha na empresa, em anos:")]
        public Enumerators.TempoServicoEmpresaAnosEnum TempoTrabalhoEmpresa { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Em sua opinião, o seu trabalho é fisicamente desgastante:")]
        public Enumerators.SimNaoUmPoucoMuitoEnum TrabalhoFisicamenteDesgastante { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Em sua opinião, o seu trabalho é psicologicamente desgastante:")]
        public Enumerators.SimNaoUmPoucoMuitoEnum TrabalhoPsicologicamenteExtressanete { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Qual seu consumo de álcool por semana? ")]
        public Enumerators.ConsumoAlcoolEnum ConsumoAlcoolSemanal { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você fuma, ou já fumou?")]
        public Enumerators.FumanteEnum Fumante { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você pratica atividades físicas?")]
        public Enumerators.SimNaoEnum PraticaAtividadeFisica { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Intensidade")]
        public Enumerators.IntensidadeAtividadesEnum Intensidade { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Quantas vezes por semana?")]
        public Enumerators.QuantidadeAtividadesEnum AtividadeFisicaIntensa { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você está satisfeito com o seu trabalho?")]
        public Enumerators.SatisfacaoTrabalhoEnum SatisfacaoTrabalho { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você está satisfeito com a vida?")]
        public Enumerators.SatisfacaoVidaEnum SatisfacaoVida { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você se sente estressado?")]
        public Enumerators.EstressadoEnum Estressado { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você tem sentindo insônia, sensação de angústia ou grande tristeza?")]
        public Enumerators.InsoniaEnum Insonia { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você possui cônjuge, amigo ou parente com quem pode contar em caso de dificuldade financeira, para desabafar ou solicitar ajuda?")]
        public Enumerators.SimNaoEnum ProximoDificuldadeFinanceira { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Em geral você diria que sua saúde é:")]
        public Enumerators.ClassificacaoSaudeEnum Saude { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você tem alguma doença que limite suas atividades em casa ou no trabalho?")]
        public Enumerators.SimNaoEnum DoencaLimiteAtividade { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Já houve necessidade de se afastar do trabalho por motivos de saúde ?")]
        public Enumerators.AfastadoPeloInss AfastamentoTrabalhoSaude { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você agenda suas consultas fora do horário de expediente?")]
        public Enumerators.ConsultaForaExpedienteEnum AgendaConsultasForaHorarioExp { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você sente dores contínuas nas costas, braços ou pernas?")]
        public Enumerators.SimNaoEnum DoresContinuaCostas { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Quantas porções por dia você consome dos seguintes alimentos: pão, bolacha, doces, refrigerante, mandioca, batata, arroz, macarrão, chipa, pão de queijo, tapioca, bolo? ")]
        public Enumerators.QuantidadePorcoesEnum QtdePorcoesConsumidaPorDia { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Quantas porções por dia você consome de frutas ou legumes ou verduras:")]
        public Enumerators.QuantidadeFrutasEnum QtdePorcoesFrutaLegumesPorDia { get; set; }
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você toma algum medicamento de Uso Contínuo?")]
        public Enumerators.SimNaoEnum TomaMedicamento { get; set; }
        [Display(Name = "Você tem alguma dessas doenças?")]
        public List<int?> Doencas { get; set; }
        [Display(Name = "Tipo de remédio?")]
        public List<int?> TipoMedicamento { get; set; }
    }


    public class LoginUsuariaoViewModel
    {
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "CPF")]
        public string CPF { get; set; }
    }

    public class TesteViewModel
    {
        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Você tem alguma dessas doenças?")]
        public List<Enumerators.DoencasEnum> Doencas { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório.")]
        [Display(Name = "Tipo de remédio?")]
        public List<Enumerators.TipodeMedicacaoEnum> TipoMedicamento { get; set; }
    }
}