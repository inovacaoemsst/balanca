﻿using System;

namespace Balanca.ViewModels
{
    public class UsuarioViewModel
    {
        public int UsuarioID { get; set; }
        public int EmpresaID { get; set; }
        public string CPF { get; set; }
        public string Nome { get; set; }
        public int Sexo { get; set; }
        public string Casa { get; set; }
        public string Setor { get; set; }
        public string Cargo { get; set; }
        public DateTime DataNascimento { get; set; }
    }
}