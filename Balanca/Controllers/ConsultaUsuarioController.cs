﻿using System;
using System.Linq;
using System.Web.Management;
using System.Web.Mvc;
using AutoMapper;
using Balanca.Common.Enumerators;
using Balanca.Entities.Balanca.Model;
using Balanca.Entities.Classes;
using Balanca.Entities.Repositorio;
using Balanca.ViewModels;

namespace Balanca.Controllers
{
    public class ConsultaUsuarioController : BaseController
    {
        private BalancaEntities db = new BalancaEntities();
        public ConsultaWsRepository repoConsultaWs = new ConsultaWsRepository();


        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginUsuariaoViewModel instancia)
        {
            if (ModelState.IsValid)
            {
                if (!String.IsNullOrEmpty(instancia.CPF))
                {

                    if (BuscarUsuarioBalanca(Convert.ToInt64(instancia.CPF)) != null || BuscarFuncionarioSoc(instancia.CPF) != null)
                        return RedirectToAction("Create", new { id = instancia.CPF });
                }

                Erro("Tentativa de login inválida, CPF não encontrado.", true);

            }

            return View(instancia);
        }


        public ActionResult Create(string id = "0")
        {
            UsuariosBalanca usuario = BuscarUsuarioBalanca(Convert.ToInt64(id));

            if (usuario != null)
            {
                ConsultaUsuarioViewModels u = Mapper.Map<UsuariosBalanca, ConsultaUsuarioViewModels>(usuario);
                u.Idade = CalculaIdade(u.DataNascimento);
                return View(u);
            }

            DadosFuncionarioSOC funcionario = BuscarFuncionarioSoc(id);
            if (funcionario != null)
            {
                ConsultaUsuarioViewModels c = new ConsultaUsuarioViewModels();
                c.Nome = funcionario.NOME;
                c.Setor = funcionario.NOMESETOR;
                c.Cargo = funcionario.NOMECARGO;
                c.DataNascimento = Convert.ToDateTime(funcionario.DATA_NASCIMENTO);
                c.Idade = CalculaIdade(c.DataNascimento);
                c.NomeEmpresa = funcionario.NOMEEMPRESA;
                c.CPF = funcionario.CPFFUNCIONARIO;
                c.Matricula = funcionario.MATRICULAFUNCIONARIO;
                c.CODIGOEMPRESA = (int)funcionario.CODIGOEMPRESA;
                c.CODIGOUNIDADE = funcionario.CODIGOUNIDADE.ToString();

                return View(c);
            }

            Erro("Tentativa de login inválida, CPF não encontrado.", true);
            return RedirectToAction("Login");
        }

        public string RetornarSexo(int codigoSexo)
        {
            return (codigoSexo == 1) ? "Masculino" : "Feminino";
        }

        public static int CalculaIdade(DateTime dtNascimento)
        {
            int idade = DateTime.Now.Year - dtNascimento.Year;
            if (DateTime.Now.Month < dtNascimento.Month || (DateTime.Now.Month == dtNascimento.Month && DateTime.Now.Day < dtNascimento.Day))
                idade--;

            return idade;
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ConsultaUsuarioViewModels instanciaView)
        {
            try
            {
                if (ModelState.IsValid && instanciaView.Doencas.Any())
                {
                    FormularioBalanca instancia = Mapper.Map<ConsultaUsuarioViewModels, FormularioBalanca>(instanciaView);
                    string numeroCracha = string.Empty;

                    EmpresasSOC emp = new EmpresaRepository().BuscarEmpresasSoc(instanciaView.CODIGOEMPRESA, instanciaView.CODIGOUNIDADE);

                    if (BuscarUsuarioBalanca(instancia.CPF) != null)
                    {
                        instancia.UsuariosBalanca = BuscarUsuarioBalanca(instancia.CPF);
                    }
                    else
                    {
                        UsuariosBalanca usuario = Mapper.Map<ConsultaUsuarioViewModels, UsuariosBalanca>(instanciaView);
                        usuario.DataCadastro = DateTime.Now;
                        usuario.Situacao = (int)Enumerators.StatusEnum.Ativo;
                        usuario.EmpresaID = emp.EmpresaID;
                        usuario.Sexo = (int)instanciaView.Sexo;
                        instancia.UsuariosBalanca = new UsuariosBalanca();
                        instancia.UsuariosBalanca = usuario;
                    }

                    instancia.UsuariosBalanca.EmpresasSOC = null;
                    instancia.DataCadastro = DateTime.Now;
                    instancia.Situacao = (int)Enumerators.StatusEnum.Ativo;



                    foreach (var doenca in instanciaView.Doencas)
                    {
                        if (doenca != null)
                        {
                            DoencasUsuario d = new DoencasUsuario { Valor = doenca.Value };
                            instancia.DoencasUsuario.Add(d);
                        }
                    }


                    if (instanciaView.TomaMedicamento == Enumerators.SimNaoEnum.Sim)
                    {
                        foreach (var medicao in instanciaView.TipoMedicamento)
                        {
                            if (medicao != null)
                            {
                                TipoMedicaoUsuario t = new TipoMedicaoUsuario { Valor = medicao.Value };
                                instancia.TipoMedicaoUsuario.Add(t);
                            }
                        }
                    }

                    db.FormularioBalanca.Add(instancia);
                    db.SaveChanges();

                    PessoaBalanca p = repoConsultaWs.CadastrarPessoaBalanca(instancia.UsuarioID, out string _msg);
                    if (p != null)
                    {
                        numeroCracha = emp.CodigoFolha + p.matricula.PadLeft(4, '0');
                        repoConsultaWs.CadastrarCrachaBalanca(p.idPessoa, numeroCracha);
                    }
                    else
                    {
                        Erro("Aviso : " + _msg, true);
                    }

                    Successo("Formulário salvo com sucesso, suba  na balança para continuar sua avaliação. ", true);
                    return RedirectToAction("Login");
                }
            }
            catch (Exception e)
            {
                Erro("Erro ao Salvar os dados :(" + e.Message);
                return View(instanciaView);
            }

            return View(instanciaView);
        }




        public DadosFuncionarioSOC BuscarFuncionarioSoc(string cpf)
        {
            return db.DadosFuncionarioSOC.FirstOrDefault(fs => fs.CPFFUNCIONARIO == cpf && fs.SITUACAO == "Ativo");
        }

        public UsuariosBalanca BuscarUsuarioBalanca(long cpf)
        {
            return db.UsuariosBalanca.FirstOrDefault(x => x.CPF == cpf);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
