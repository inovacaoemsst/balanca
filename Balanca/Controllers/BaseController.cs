﻿using System.Collections.Generic;
using System.Web.Mvc;

using Balanca.Entities.Classes;

namespace Balanca.Controllers
{
    public class BaseController : Controller
    {
        public void Successo(string message, bool dismissable = false)
        {
            AddErro(AlertStyles.Successo, message, dismissable);
        }

        public void Erro(string message, bool dismissable = false)
        {
            AddErro(AlertStyles.Erro, message, dismissable);
        }

        private void AddErro(string alertStyle, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey)
                ? (List<Alert>)TempData[Alert.TempDataKey]
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                Message = message,
                Dismissable = dismissable
            });

            TempData[Alert.TempDataKey] = alerts;
        }

    }
}