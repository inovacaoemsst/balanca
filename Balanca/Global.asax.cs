﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using Balanca.Entities.Balanca.Model;
using Balanca.ViewModels;

namespace Balanca
{
    public class MvcApplication : System.Web.HttpApplication
    {

        public static void InitializeMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ConsultaUsuarioViewModels, FormularioBalanca>().ReverseMap();
                cfg.CreateMap<ConsultaUsuarioViewModels, UsuariosBalanca>().ReverseMap();
                cfg.CreateMap<UsuariosBalanca, ConsultaUsuarioViewModels>().ForMember(dest => dest.NomeEmpresa, opt => opt.MapFrom(src => src.EmpresasSOC.NOMEEMPRESA)).ReverseMap();

            });
        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitializeMapper();

        }
    }
}
