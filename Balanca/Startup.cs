﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Balanca.Startup))]
namespace Balanca
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
