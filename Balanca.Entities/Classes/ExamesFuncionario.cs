﻿using System;

namespace Balanca.Entities.Classes
{
    public class ExamesFuncionario
    {
        public string nomeExame { get; set; }
        public string codigoExame { get; set; }
        public string nomeEmpresa { get; set; }
        public string dataResultadoExame { get; set; }
        public string dataRefazerExame { get; set; }
        public string periodicidade { get; set; }
    }
}
