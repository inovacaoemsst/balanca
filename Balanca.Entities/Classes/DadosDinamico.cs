﻿namespace Balanca.Entities.Classes
{
    public class DadosDinamico
    {
        public string NOMECADASTROGENERICO { get; set; }
        public string CODIGOEMPRESA { get; set; }
        public string RESPOSTA { get; set; }
        public string ITEM { get; set; }
        public string NOMECADASTRO { get; set; }
        public string CODIGOCADASTROGENERICO { get; set; }
        public string CODIGOCADASTRO { get; set; }
        public string CODIGOTIPOPERSONALIZACAO { get; set; }
    }
}
