﻿

namespace Balanca.Entities.Classes
{
    public class Empresa
    {
        public string COMPLEMENTOENDERECO { get; set; }
        public string BAIRRO { get; set; }
        public string RAZAOSOCIAL { get; set; }
        public string CODIGO { get; set; }
        public string APELIDO { get; set; }
        public string INSCRICAOMUNICIPAL { get; set; }
        public string UF { get; set; }
        public string NOME { get; set; }
        public string ATIVO { get; set; }
        public string CEP { get; set; }
        public string CODIGOCLIENTEINTEGRACAO { get; set; }
        public string ENDERECO { get; set; }
        public string INSCRICAOESTADUAL { get; set; }
        public string CIDADE { get; set; }
        public string NUMEROENDERECO { get; set; }
        public string CNPJ { get; set; }
    }
}