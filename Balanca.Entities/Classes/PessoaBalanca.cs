﻿using System;
using System.Collections.Generic;

namespace Balanca.Entities.Classes
{
    public class PessoaBalanca
    {
        public int idPessoa { get; set; }
        public string tipo { get; set; }
        public string nome { get; set; }
        public long cpf { get; set; }
        public string altura { get; set; }
        public string rg { get; set; }
        public string telefone { get; set; }
        public DateTime dtNascimento { get; set; }
        public string matricula { get; set; }
        public string email { get; set; }
        public string pis { get; set; }
        public object idFuncao { get; set; }
        public object idGrupo { get; set; }
        public string sexo { get; set; }
        public string situacao { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public int idEmpresa { get; set; }
        public bool bloqueado { get; set; }
    }

    public class RetornoWsGetPessoasBalanca
    {
        public int total { get; set; }
        public List<PessoaBalanca> Pessoas = new List<PessoaBalanca>();
    }

    public class RetornoWsCriarPessoasBalanca
    {
        public string message { get; set; }
        public PessoaBalanca Value = new PessoaBalanca();
    }

    
}