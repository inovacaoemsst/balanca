﻿namespace Balanca.Entities.Classes
{
    public class AsoSocial
    {
        public string OBSPROC { get; set; }
        public string FRMCTTSERVSAUDE { get; set; }
        public string CODIGOEMPRESA { get; set; }
        public string NMMED { get; set; }
        public string MATRICULA { get; set; }
        public string NMTRAB { get; set; }
        public string CPFTRAB { get; set; }
        public string CODIGOFUNCIONARIO { get; set; }
        public string RESASOSOC { get; set; }
        public string INDRESULTADOALTNORMAL { get; set; }
        public string PROCREALIZADO { get; set; }
        public string DTFIMMONIT { get; set; }
        public string NISTRAB { get; set; }
        public string DTASO { get; set; }
        public string RESASO { get; set; }
        public string INDRESULTADOAGRAV { get; set; }
        public string TPINSCEMPRESA { get; set; }
        public string DTINIMONIT { get; set; }
        public string DTEXAME { get; set; }
        public string EMAILSERVSAUDE { get; set; }
        public string DESCRICAOEXAME { get; set; }
        public string INDRESULTADOESTAVEL { get; set; }
        public string TPASO { get; set; }
        public string TPINSCUNIDADE { get; set; }
        public string NRINSCEMPRESAUNIDADE { get; set; }
        public string UFCRM { get; set; }
        public string DATAFICHA { get; set; }
        public string NRCRM { get; set; }
        public string NRINSCEMPRESA { get; set; }
        public string INTERPREEXM { get; set; }
        public string IDFICHA { get; set; }
        public string ORDEXAME { get; set; }
        public string DTULTALTASO { get; set; }
        public string CODIGOMEDICO { get; set; }
        public string DTINCLUSAOPARECERASO { get; set; }
    }
}
