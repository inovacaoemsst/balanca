﻿namespace Balanca.Entities.Classes
{
    public class Alert
    {
        public const string TempDataKey = "TempDataAlerts";

        public string AlertStyle { get; set; }
        public string Message { get; set; }
        public bool Dismissable { get; set; }
    }

    public static class AlertStyles
    {
        public const string Successo = "success";
        public const string Informacao = "info";
        public const string Alerta = "warning";
        public const string Erro = "danger";
    }
}