﻿using System;

namespace Balanca.Entities.Classes
{
    public class CrachaBalanca
    {
        public string tipoCracha { get; set; }
        public string numero { get; set; }
        public DateTime dataInicio { get; set; }
        public DateTime dataBaixa { get; set; }
        public string situacao { get; set; }
    }
}