﻿namespace Balanca.Entities.Classes
{
    public class Deficiencia
    {
        public string NOMEFUNCIONARIO { get; set; }
        public string CODFUNCIONARIO { get; set; }
        public string CODIGOTIPODEFICIENCIA { get; set; }
        public string DEFICIENCIA { get; set; }
        public string MATRICULAFUNCIONARIO { get; set; }
        public string EMPRESA { get; set; }
        public string CLASSIFICAO { get; set; }
    }
}
