﻿namespace Balanca.Entities.Classes
{
    public class FuncionarioSoc
    {
        public string TELEFONERESIDENCIAL { get; set; }
        public string BAIRRO { get; set; }
        public string CODIGOCARGO { get; set; }
        public string CODIGO { get; set; }
        public string RG { get; set; }
        public string DEFICIENTE { get; set; }
        public string CODIGOEMPRESA { get; set; }
        public string CTPS { get; set; }
        public string NOME { get; set; }
        public string DATAULTALTERACAO { get; set; }
        public string NUMERO_ENDERECO { get; set; }
        public string ESTADOCIVIL { get; set; }
        public string PIS { get; set; }
        public string SERIECTPS { get; set; }
        public string SEXO { get; set; }
        public string EMAIL { get; set; }
        public string DATA_ADMISSAO { get; set; }
        public string ENDERECO { get; set; }
        public string NOMECENTROCUSTO { get; set; }
        public string NOMESETOR { get; set; }
        public string CBOCARGO { get; set; }
        public string CCUSTO { get; set; }
        public string CODIGOSETOR { get; set; }
        public string DATA_DEMISSAO { get; set; }
        public string NM_MAE_FUNCIONARIO { get; set; }
        public string UF { get; set; }
        public string NOMEUNIDADE { get; set; }
        public string NOMECARGO { get; set; }
        public string NOMEEMPRESA { get; set; }
        public string ORGAOEMISSORRG { get; set; }
        public string CEP { get; set; }
        public string TIPOCONTATACAO { get; set; }
        public string CPF { get; set; }
        public string DEFICIENCIA { get; set; }
        public string MATRICULAFUNCIONARIO { get; set; }
        public string SITUACAO { get; set; }
        public string CODIGOUNIDADE { get; set; }
        public string DATA_NASCIMENTO { get; set; }
        public string TELEFONECELULAR { get; set; }
        public string CIDADE { get; set; }
        public string UFRG { get; set; }
    }
}