﻿namespace Balanca.Entities.Classes
{
    public class ContatoEmpresa
    {
        public string EMAIL2 { get; set; }
        public string NOMECONTATO { get; set; }
        public string CODIGOCONTATO { get; set; }
        public string EMAIL1 { get; set; }
        public string RAMAL2 { get; set; }
        public string RAMAL1 { get; set; }
        public string CODIGOEMPRESA { get; set; }
        public string TEL1 { get; set; }
        public string TEL2 { get; set; }

    }
}
