﻿namespace Balanca.Entities.Classes
{
    public class AsosPeriodoEmpresa
    {
        public string NOMEFUNCIONARIO { get; set; }
        public string CODPARECERASO { get; set; }
        public string DTASO { get; set; }
        public string TPASO { get; set; }
        public string NOMERESPONSAVELASO { get; set; }
        public string CODIGOFUNCIONARIO { get; set; }
        public string UF { get; set; }
        public string CRM { get; set; }
        public string CPFFUNCIONARIO { get; set; }
    }
}
