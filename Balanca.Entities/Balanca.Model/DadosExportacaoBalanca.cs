//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Balanca.Entities.Balanca.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class DadosExportacaoBalanca
    {
        public int ConsutaBalancaID { get; set; }
        public int UsuarioID { get; set; }
        public Nullable<int> IdMedico { get; set; }
        public string PressaoMax { get; set; }
        public string PressaoMin { get; set; }
        public string Pulso { get; set; }
        public string IMC { get; set; }
        public string IGP { get; set; }
        public string Peso { get; set; }
        public string DataEquipamento { get; set; }
        public string Equipamento { get; set; }
        public Nullable<System.DateTime> DataConsultaBalanca { get; set; }
        public System.DateTime DataCadastro { get; set; }
        public int CPF { get; set; }
        public int Situacao { get; set; }
    }
}
