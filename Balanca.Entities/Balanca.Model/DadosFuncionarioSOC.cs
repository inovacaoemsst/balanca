//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Balanca.Entities.Balanca.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class DadosFuncionarioSOC
    {
        public int FuncSocID { get; set; }
        public Nullable<double> CODIGOEMPRESA { get; set; }
        public string NOMEEMPRESA { get; set; }
        public Nullable<double> CODIGO { get; set; }
        public string NOME { get; set; }
        public Nullable<double> CODIGOUNIDADE { get; set; }
        public string NOMEUNIDADE { get; set; }
        public Nullable<double> CODIGOSETOR { get; set; }
        public string NOMESETOR { get; set; }
        public Nullable<double> CODIGOCARGO { get; set; }
        public string NOMECARGO { get; set; }
        public string CBOCARGO { get; set; }
        public string MATRICULAFUNCIONARIO { get; set; }
        public string CPFFUNCIONARIO { get; set; }
        public string SITUACAO { get; set; }
        public Nullable<System.DateTime> DATA_NASCIMENTO { get; set; }
        public Nullable<System.DateTime> DATA_ADMISSAO { get; set; }
        public Nullable<System.DateTime> DATA_DEMISSAO { get; set; }
        public string ENDERECO { get; set; }
        public string NUMERO_ENDERECO { get; set; }
        public string BAIRRO { get; set; }
        public string UF { get; set; }
    }
}
