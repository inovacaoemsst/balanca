//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Balanca.Entities.Balanca.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class DadosContatoEmpresasSOC
    {
        public double ContatosID { get; set; }
        public int CODIGOEMPRESA { get; set; }
        public Nullable<double> CODIGOCONTATO { get; set; }
        public string NOMECONTATO { get; set; }
        public string TEL1 { get; set; }
        public string RAMAL1 { get; set; }
        public string TEL2 { get; set; }
        public string RAMAL2 { get; set; }
        public string EMAIL1 { get; set; }
        public string EMAIL2 { get; set; }
        public Nullable<int> EmpresaID { get; set; }
    }
}
