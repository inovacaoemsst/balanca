﻿using Balanca.Entities.Balanca.Model;

namespace Balanca.Entities.Repositorio
{
    interface IEmpresaRepository : IRepositorio<EmpresasSOC>
    {
    }
}
