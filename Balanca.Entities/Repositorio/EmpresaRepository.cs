﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Balanca.Entities.Balanca.Model;
using Balanca.Entities.Classes;

namespace Balanca.Entities.Repositorio
{
   
    public class EmpresaRepository 
    {

        BalancaEntities db = new BalancaEntities();

        public void Adicionar(EmpresasSOC entity)
        {
            throw new NotImplementedException();
        }

        public void Atualizar(EmpresasSOC entity)
        {
            throw new NotImplementedException();
        }

        public void Commit()
        {
            throw new NotImplementedException();
        }

        public void Deletar(Func<EmpresasSOC, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Empresa> Get(Expression<Func<EmpresasSOC, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Empresa> GetTodos()
        {
            throw new NotImplementedException();
        }

        public EmpresasSOC Primeiro(Expression<Func<EmpresasSOC, bool>> predicate)
        {
            return db.EmpresasSOC.FirstOrDefault(predicate);
        }

        public EmpresasSOC Procurar(params object[] key)
        {
            throw new NotImplementedException();
        }

        public EmpresasSOC  BuscarEmpresasSoc(int codEmpresa, string codUnidade)
        {
           return this.Primeiro(x => x.CODIGOEMPRESA == codEmpresa && x.CODIGOUNIDADE == codUnidade);
        }

    }

}

