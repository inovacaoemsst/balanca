﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Balanca.Entities.Balanca.Model;
using Balanca.Entities.Classes;
using Newtonsoft.Json;
using RestSharp;
using Empresa = Balanca.Entities.Classes.Empresa;


namespace Balanca.Entities.Repositorio
{
    public class ConsultaWsRepository
    {
      
        public List<EmpresasSOC> ListEmpresaSoc = new List<EmpresasSOC>();
        public List<DadosContatoEmpresasSOC> ListContatosEmpresaSoc = new List<DadosContatoEmpresasSOC>();
        public List<DadosDinamico> ListDadosDinamico = new List<DadosDinamico>();
        public List<AsosPeriodoEmpresa> ListAsosPeriodoEmpresa = new List<AsosPeriodoEmpresa>();
        public List<Deficiencia> ListDeficienciaEmpresa = new List<Deficiencia>();
        public List<AsoSocial> ListDadosEsocialEmpresa = new List<AsoSocial>();
        public List<ChavesEmpresasExpDadosSOC> ListChaveEmpresasSoc = new List<ChavesEmpresasExpDadosSOC>();
        public List<ExamesFuncionarioSOC> ListExamesFuncionarios = new List<ExamesFuncionarioSOC>();
        public static string ResultadoJson = String.Empty;
        public List<FuncionarioSoc> ListFuncionarios = new List<FuncionarioSoc>();
        public List<PessoaBalanca> ListPessoasBalanca = new List<PessoaBalanca>();
        public BalancaEntities db = new BalancaEntities();

        public static string TokenApi = ConfigurationManager.AppSettings["Token_API_Balanca"];
        public static string UrlApi = ConfigurationManager.AppSettings["URL_API_Balanca"];

       

        public bool CriarCrachaUsuariosBalancaWs(string path, string body = "")
        {
            IRestResponse reposta = RealizarConsultaWsBalanca(Method.POST, path, body);
            ResultadoJson = reposta.Content;
            return (reposta.StatusCode == HttpStatusCode.OK);
        }

        public PessoaBalanca CriarUsuariosBalancaWs(string path, string body, out string mensagemErro)
        {
            IRestResponse reposta = RealizarConsultaWsBalanca(Method.POST, path, body);
            ResultadoJson = reposta.Content;

            if (reposta.StatusCode == HttpStatusCode.OK)
            {
                var retorno = JsonConvert.DeserializeObject<RetornoWsCriarPessoasBalanca>(ResultadoJson);
                mensagemErro = retorno.message;
                return retorno.Value;
            }
            else if (reposta.StatusCode == HttpStatusCode.InternalServerError)
            {
                mensagemErro = "Erro interno no servidor" + reposta.Content;
            }
            else if (reposta.StatusCode.ToString() == "422")
            {
                mensagemErro = "Pessoa já cadastrada";
            }
            else
            {
                mensagemErro = "Erro ao cadastrar pessoa na balança";
            }


            return null;
        }

        public CrachaBalanca CriarCrachaBalancaWs(string path, string body, out string mensagemErro)
        {
            IRestResponse reposta = RealizarConsultaWsBalanca(Method.POST, path, body);
            ResultadoJson = reposta.Content;

            if (reposta.StatusCode == HttpStatusCode.OK)
            {
                var retorno = JsonConvert.DeserializeObject<CrachaBalanca>(ResultadoJson);
                mensagemErro = "Pessoa Cadastrada";
                return retorno;
            }
            else if (reposta.StatusCode == HttpStatusCode.InternalServerError)
            {
                mensagemErro = "Erro interno no servidor" + reposta.Content;
            }
            else if (reposta.StatusCode.ToString() == "422")
            {
                mensagemErro = "Pessoa já cadastrada";
            }
            else
            {
                mensagemErro = "Erro ao cadastrar pessoa na balança";
            }


            return null;
        }

        //public void RetornarDadosExameFuncionarioaWs(string parametros)
        //{
           
        //    ResultadoJson = RealizarConsultaExportaDadosSoc(parametros);
        //    if (!string.IsNullOrEmpty(ResultadoJson))
        //    {
        //        ListExamesFuncionarios = new List<ExamesFuncionario>();
        //        ListExamesFuncionarios.AddRange(JsonConvert.DeserializeObject<List<ExamesFuncionario>>(ResultadoJson));
        //    }
        //}

        public void RetornarDadosExameFuncionario(string estadoDr, int empresaSocID)
        {
            ListExamesFuncionarios = db.ExamesFuncionarioSOC.Where( x => x.EmpresaSocID == empresaSocID).ToList();

        }


        public List<PessoaBalanca> RetornaUsuariosBalancaWs(string path)
        {
            IRestResponse reposta = RealizarConsultaWsBalanca(Method.GET, path);
            ResultadoJson = reposta.Content;
            var result = new RetornoWsGetPessoasBalanca();

            if (!string.IsNullOrEmpty(ResultadoJson))
            {
                result = JsonConvert.DeserializeObject<RetornoWsGetPessoasBalanca>(ResultadoJson);
            }

            return result?.Pessoas;
        }

        public List<CrachaBalanca> RetornaCrachaUsuariosBalancaWs(string path)
        {
            IRestResponse reposta = RealizarConsultaWsBalanca(Method.GET, path);
            ResultadoJson = reposta.Content;
            var result = new List<CrachaBalanca>();
            if (!string.IsNullOrEmpty(ResultadoJson))
            {
                result = JsonConvert.DeserializeObject<List<CrachaBalanca>>(ResultadoJson);
            }
            return result;
        }

        public void RetornarDadosAsoeSocialaWs(string parametros)
        {
            ResultadoJson = RealizarConsultaExportaDadosSoc(parametros);
            if (!string.IsNullOrEmpty(ResultadoJson))
            {
                ListDadosEsocialEmpresa = new List<AsoSocial>();
                ListDadosEsocialEmpresa.AddRange(JsonConvert.DeserializeObject<List<AsoSocial>>(ResultadoJson));
            }
        }

        public void RetornarAsosPeriodoEmpresaWs(string parametros)
        {
            ResultadoJson = RealizarConsultaExportaDadosSoc(parametros);

            if (!string.IsNullOrEmpty(ResultadoJson))
            {
                ListAsosPeriodoEmpresa = new List<AsosPeriodoEmpresa>();
                ListAsosPeriodoEmpresa.AddRange(JsonConvert.DeserializeObject<List<AsosPeriodoEmpresa>>(ResultadoJson));
            }
        }

        public void RetornarDadosDinamicosWs(string parametros)
        {
            ResultadoJson = RealizarConsultaExportaDadosSoc(parametros);
            if (!string.IsNullOrEmpty(ResultadoJson))
            {
                ListDadosDinamico = new List<DadosDinamico>();
                ListDadosDinamico.AddRange(JsonConvert.DeserializeObject<List<DadosDinamico>>(ResultadoJson));
            }
        }

       

        public void RetornaFuncionariosSocWs(string parametros)
        {
            ResultadoJson = RealizarConsultaExportaDadosSoc(parametros);
            if (!string.IsNullOrEmpty(ResultadoJson))
            {
                ListFuncionarios = new List<FuncionarioSoc>();
                ListFuncionarios.AddRange(JsonConvert.DeserializeObject<List<FuncionarioSoc>>(ResultadoJson));
            }
        }

        //public void RetornarContatosEmpresaWs(string parametros)
        //{
        //    ResultadoJson = RealizarConsultaExportaDadosSoc(parametros);
        //    if (!string.IsNullOrEmpty(ResultadoJson))
        //    {
        //        ListContatoEmpresa = new List<ContatoEmpresa>();
        //        ListContatoEmpresa.AddRange(JsonConvert.DeserializeObject<List<ContatoEmpresa>>(ResultadoJson));
        //    }
        //}

        //public void RetornaEmpresasSocWs(string parametros)
        //{
        //    ResultadoJson = RealizarConsultaExportaDadosSoc(parametros);
        //    if (!string.IsNullOrEmpty(ResultadoJson))
        //    {
        //        ListEmpresas = new List<Empresa>();
        //        ListEmpresas.AddRange(JsonConvert.DeserializeObject<List<Empresa>>(RealizarConsultaExportaDadosSoc(parametros)));
        //    }
        //}

        public void RetornaEmpresaSoc(string estadoDR)
        {
          ListEmpresaSoc = db.EmpresasSOC.Where(x => x.UF == estadoDR).ToList();
        }

        public void RetornarContatosEmpresaSoc()
        {
            ListContatosEmpresaSoc = db.DadosContatoEmpresasSOC.ToList();
        }

        private static string RealizarConsultaExportaDadosSoc(string parametros)
        {
            System.Net.HttpWebRequest webrequest = (HttpWebRequest)System.Net.WebRequest.Create("https://www.soc.com.br/WebSoc/exportadados?" + parametros);

            webrequest.Method = "POST";
            webrequest.ContentType = "application/json";
            webrequest.ContentLength = 0;


            string result = string.Empty;

            Stream stream = webrequest.GetRequestStream();
            stream.Close();

            using (WebResponse response = webrequest.GetResponse())
            {

                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.Default, true))
                {
                    result = reader.ReadToEnd();
                    if (result == "Problemas com a chave ou empresa")
                    {
                        result = String.Empty;
                    }

                }
            }
            return result;
        }

        private static IRestResponse RealizarConsultaWsBalanca(Method verboMethod, string path, string body = "")
        {
            var client = new RestClient(UrlApi);
            var request = new RestRequest(path, verboMethod);

            if (verboMethod == Method.POST)
            {
                request.Parameters.Clear();
                request.AddParameter("application/json", body, ParameterType.RequestBody);
            }
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", TokenApi);
            return client.Execute(request);
        }

        public PessoaBalanca CadastrarPessoaBalanca(int idUsuario, out string _msg)
        {
            UsuariosBalanca usuario = db.UsuariosBalanca.FirstOrDefault(x => x.UsuarioID == idUsuario);
            PessoaBalanca pessoa = new PessoaBalanca();

            pessoa.tipo = "funcionario";
            pessoa.cpf = usuario.CPF;
            pessoa.idEmpresa = usuario.EmpresaID;
            pessoa.nome = usuario.Nome;
            pessoa.sexo = (usuario.Sexo == 1) ? "M" : "F";
            pessoa.matricula = usuario.Matricula.ToString();
            pessoa.dtNascimento = usuario.DataNascimento.Value;
            pessoa.situacao = "1";
            pessoa.createdAt = DateTime.Now;

            string pessoaJson = JsonConvert.SerializeObject(pessoa);
            pessoa = CriarUsuariosBalancaWs("pessoas", pessoaJson, out var mensagem);
            _msg = mensagem;
            return pessoa;
        }

        public bool CadastrarCrachaBalanca(int idPessoaBalanca, string numeroCracha)
        {
           string path = "pessoas/" + idPessoaBalanca + "/crachas";
            CrachaBalanca crachaBalanca = new CrachaBalanca();
            crachaBalanca.situacao = "1";
            crachaBalanca.dataInicio = DateTime.Now;
            crachaBalanca.numero = numeroCracha;
            crachaBalanca.tipoCracha = "1";
            string crachaBalancaJson = JsonConvert.SerializeObject(crachaBalanca);
            return CriarCrachaUsuariosBalancaWs(path, crachaBalancaJson);
        }

    }
}