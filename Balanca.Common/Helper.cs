﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;


namespace Balanca.Common
{
    public static class Helper
    {
        public static MvcHtmlString EnumRadioButton<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            ModelMetadata metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            string[] listOfValues = Enum.GetNames(metaData.ModelType);

            StringBuilder sb = new StringBuilder();

            if (listOfValues != null)            {


                foreach (string name in listOfValues)
                {
                    sb = sb.AppendFormat("<label class='btn btn-primary not-active'>");

                    string label = name;

                    MemberInfo[] memInfo = metaData.ModelType.GetMember(name);



                    if (memInfo != null)
                    {
                        IList<CustomAttributeData> attributes = memInfo[0].GetCustomAttributesData();

                        if (attributes != null && attributes.Any())
                        {
                            label = attributes[0].ConstructorArguments[0].Value.ToString();
                        }
                    }

                    //< input type = "checkbox" value = "1" name = "Doenca[]" id = "campo-checkbox1" />
                    //    < label for= "campo-checkbox1" > Opção </ label >

                    string id = string.Format("{0}_{1}_{2}", htmlHelper.ViewData.TemplateInfo.HtmlFieldPrefix, metaData.PropertyName, name);

                    string radio = htmlHelper.RadioButtonFor(expression, name, new { id = id }).ToHtmlString();

                    sb.AppendFormat("{0}{1}", radio, HttpUtility.HtmlEncode(label));
                    sb = sb.AppendFormat("</label>");
                }


            }

            return MvcHtmlString.Create(sb.ToString());
        }


        public static MvcHtmlString EnumCheckbox<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            ModelMetadata metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            string[] listOfValues = Enum.GetNames(metaData.ModelType);

            StringBuilder sb = new StringBuilder();

            if (listOfValues != null)
            {


                foreach (string name in listOfValues)
                {


                    string label = name;

                    MemberInfo[] memInfo = metaData.ModelType.GetMember(name);



                    if (memInfo != null)
                    {
                        IList<CustomAttributeData> attributes = memInfo[0].GetCustomAttributesData();

                        if (attributes != null && attributes.Any())
                        {
                            label = attributes[0].ConstructorArguments[0].Value.ToString();
                        }
                    }



                    string id = $"{htmlHelper.ViewData.TemplateInfo.HtmlFieldPrefix}_{metaData.PropertyName}_{name}";

                    if (expression != null)
                    {
                        string checkbox = htmlHelper.CheckBox(expression.Body.ToString(), new { id = id, value = name, name = "model.Doenca[]" }).ToHtmlString();

                        sb.AppendFormat("<div>{0} {1}</div>", checkbox, HttpUtility.HtmlEncode(label));
                    }
                }


            }

            return MvcHtmlString.Create(sb.ToString());
        }

       


    }
}