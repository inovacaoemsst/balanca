﻿using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Balanca.Common.Enumerators
{
    public class Enumerators
    {
        public enum SexoEnum
        {
            [Description("Masculino")]
            Masculino = 1,
            [Description("Feminino")]
            Feminino = 2
        }

        public enum StatusEnum
        {
            [Description("Ativo")]
            Ativo = 1,
            [Description("Desativado")]
            Desativado = 2
        }

        public enum EscolaridadeEnum
        {
            [Description("Ensino fundamental")]
            EnsinoFundamental = 1,
            [Description("Ensino médio")]
            EnsinoMedio = 2,
            [Description("Ensino superior")]
            EnsinoSuperior = 3,
            [Description("Pós Graduação")]
            PosGraduacao = 4,
            [Description("Mestrado")]
            Mestrado = 5
        }


        public enum EstadoCivilEnum
        {
            [Description("Casado (a)")]
            Casado = 1,
            [Description("Solteiro (a)")]
            Solteiro = 2,
            [Description("Viúvo (a)")]
            Viuvo = 3,
            [Description("Divorciado (a)")]
            PosGraduacao = 4,
            [Description("União Estável (a)")]
            UniaoEstavel = 5
        }


        public enum FilhosEnum
        {
            [Description("Não")]
            Nao = 1,
            [Description("Sim, 01 filho")]
            SimUmFilho = 2,
            [Description("Sim, 02 a 03 Filhos")]
            SimDoisaTresFilhos = 3,
            [Description("Sim, mais de 03 filhos")]
            SimMaisdeTresFilhos = 4
        }

        public enum TempoServicoEmpresaAnosEnum
        {
            [Description("Menos de 1 ano")]
            MenosdeUmAno = 1,
            [Description("De 1 a 5 anos")]
            DeUmACincoAnos = 2,
            [Description("Mais de 5 anos")]
            MaisDeCincoAnos = 3
        }

        public enum SimNaoUmPoucoMuitoEnum
        {
            [Description("Não")]
            Nao = 1,
            [Description("Um Pouco")]
            UmPouco = 2,
            [Description("Muito")]
            Muito = 3
        }

        public enum SimNaoEnum
        {
            [Description("Sim")]
            Sim = 1,
            [Description("Não")]
            Nao = 2
        }

        public enum ConsumoAlcoolEnum
        {
            [Description("Não Consumo")]
            NaoConsumo = 1,
            [Description("de 1 a 7 doses")]
            UmaSeteDoses = 2,
            [Description("acima de 8 doses")]
            AcimadeOitoDoses = 3
        }

        public enum FumanteEnum
        {
            [Description("Sim")]
            Sim = 1,
            [Description("Não")]
            Nao = 2,
            [Description("Ex-fumante")]
            ExFumante = 3
        }


        public enum IntensidadeAtividadesEnum
        {
            [Description("Leve")]
            Leve = 1,
            [Description("Moderada")]
            Moderada = 2,
            [Description("Intensa")]
            Intensa = 3
        }


        public enum QuantidadeAtividadesEnum
        {
            [Description("01 vez")]
            Um = 1,
            [Description("02 vezes")]
            Dois = 2,
            [Description("03 ou mais")]
            TresOuMais = 3
        }

        public enum SatisfacaoTrabalhoEnum
        {
            [Description("Sim")]
            Sim = 1,
            [Description("Não")]
            Nao = 2,
            [Description("Pouco Satisfeito")]
            PoucoSatisfeito = 3
        }


        public enum SatisfacaoVidaEnum
        {
            [Description("Plenamente satisfeito")]
            PlenamenteSatisfeito = 1,
            [Description("Satisfeito")]
            Satisfeito = 2,
            [Description("insatisfeito")]
            Insatisfeito = 3,
            [Description(" Muito insatisfeito")]
            MuitoInsatisfeito = 4
        }
        public enum EstressadoEnum
        {
            [Description("Raramente")]
            Raramente = 1,
            [Description("Sim, sempre estressado;")]
            SimSempre = 2,
            [Description("Não")]
            Não = 3
        }

        public enum InsoniaEnum
        {
            [Description("Não")]
            Não = 1,
            [Description("Raramente")]
            Raramente = 2,
            [Description("Frequentemente")]
            Frequentemente = 3
        }
       
        public enum ClassificacaoSaudeEnum
        {
            [Description("Excelente")]
            Excelente = 1,
            [Description("Boa")]
            Boa = 2,
            [Description("Ruim")]
            Ruim = 3,
            [Description("Muito Ruim")]
            MuitoRuim = 4
        }



        public enum AfastadoPeloInss
        {
            [Description("Não")]
            Nao = 1,
            [Description("Sim tempo inferior a 15 dias")]
            SimInferiorQuinzeDias = 2,
            [Description("Sim, fui afastado pelo INSS (tempo maior que 15 dias)")]
            SimSuperiorQuinzeDias = 3
        }



        public enum ConsultaForaExpedienteEnum
        {
            [Description("Sim")]
            Sim = 1,
            [Description("Não")]
            Nao = 2,
            [Description("Nunca tentei")]
            NuncaTentei = 3
        }

        public enum QuantidadePorcoesEnum
        {
            [Description("Até 4 porções por dia")]
            AteQuatroPorcoes = 1,
            [Description("5 a 8 porções/ dia")]
            CincoOitoPorcoes = 2,
            [Description("acima de 8 porções por dia")]
            AcimaOitoPorcoes = 3
        }

        public enum QuantidadeFrutasEnum
        {
            [Description("Quase não consumo")]
            QuaseNaoConsumo = 1,
            [Description("1 vez por dia")]
            UmaVezPorDia = 2,
            [Description("2 a 3 vezes por dia")]
            DuasTresVezesPorDia = 3,
            [Description("Mais que 3 vezes por dia")]
            MaisQueTresVezesPorDia = 4

        }

        public enum DoencasEnum
        {
            [Description("Hipertensão arterial")]
            HipertensaoArterial = 1,
            [Description("Diabetes")]
            Diabetes = 2,
            [Description("Hipotireoidismo ou hipertireoidismo")]
            HipotireoidismoHipertireoidismo = 3,
            [Description("Colesterol alto ou triglicerídeos alto")]
            Colesterol = 4,
            [Description("Depressão, ansiedade ou doenças psiquiátricas")]
            Depressao = 5,
            [Description("Tendinite, bursite ou hérnia de disco")]
            Artrite = 6,
            [Description("Bronquite ou asma")]
            Bronquite = 7,
            [Description("Problemas cardíacos, cirurgia cardíaca ou infarto")]
            ProblemasCardiacos = 8,
            [Description("Não")]
            Nao = 9

        }

        public enum TipodeMedicacaoEnum
        {
            [Description("Pressão Alta")]
            PressaoAlta = 1,
            [Description("Diabetes")]
            Diabetes = 2,
            [Description("Colesterol")]
            Colesterol = 3,
            [Description("Anticoncepcionais")]
            Anticoncepcionais = 4,
            [Description("Remédios controlados")]
            RemediosControlados = 5,
            [Description("Outros")]
            Outros = 6
        }


        public static string GetDescription(System.Enum value)
        {
            var enumMember = value.GetType().GetMember(value.ToString()).FirstOrDefault();
            var descriptionAttribute =
                enumMember == null
                    ? default(DescriptionAttribute)
                    : enumMember.GetCustomAttribute(typeof(DescriptionAttribute)) as DescriptionAttribute;
            return
                descriptionAttribute == null
                    ? value.ToString()
                    : descriptionAttribute.Description;
        }



    }



}