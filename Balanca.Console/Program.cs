﻿using System;

using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web.UI;
using Balanca.Entities.Balanca.Model;
using Balanca.Entities.Classes;
using Balanca.Entities.Repositorio;

namespace Balanca.Console
{
    public class Program
    {
        public static string EstadoDr = string.Empty;
        public static DateTime DataInicio;
        public static DateTime DataFim;

        public static int TotalDrAdmissionalValido;
        public static int TotalDrPeriodicoValido;
        public static int TotalDrMudancaFuncaoValido;
        public static int TotalDrRetornoTrabalhoValido;
        public static int TotalDrDemissionalValido;
        public static int TotalDrretornoTrabalhoValido;
        public static int TotalDrTrabalhadoresValidos;
        public static int TotalDrDadosSeguranca;
       


        public static ConsultaWsRepository repoConsultaWs = new ConsultaWsRepository();
        public static List<string> ListLog = new List<string>();


        static void Main(string[] args)
        {

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            GerarArquivoFomento("AC", new DateTime(2018, 01, 01), new DateTime(2018, 01, 31));
            stopwatch.Stop();
            System.Console.WriteLine($"Tempo Execução : {stopwatch.Elapsed}");
            System.Console.WriteLine("Fim da Execução");
            System.Console.ReadLine();
        }

        private static void AtualizarCpfFuncionariosSoc()
        {
            BalancaEntities db = new BalancaEntities();

            foreach (var item in db.DadosFuncionarioSOC)
            {
                if (!string.IsNullOrWhiteSpace(item.CPFFUNCIONARIO))
                {
                    System.Console.WriteLine(item.CPFFUNCIONARIO + " => " + item.CPFFUNCIONARIO.PadLeft(11, '0'));
                    item.CPFFUNCIONARIO = item.CPFFUNCIONARIO.PadLeft(11, '0');
                }
            }

            db.SaveChanges();
        }

        private static void GerarArquivoFomento(string estadoDr, DateTime dataInicio, DateTime dataFinal)
        {
            EstadoDr = estadoDr;
            DataInicio = dataInicio;
            DataFim = dataFinal;

            repoConsultaWs.RetornaEmpresaSoc(estadoDr);
            repoConsultaWs.RetornarContatosEmpresaSoc();

            CarregarChaves();
            GerarArquivoRelatorioCsv(MontarArquivoRelatorioFomento());
            GerarLog(ListLog);
        }


        private static void CarregarChaves()
        {
            repoConsultaWs.ListChaveEmpresasSoc = new BalancaEntities().ChavesEmpresasExpDadosSOC.ToList();
        }

        private static bool CarregarDadosEmpresa(int codigoEmpresa)
        {
            ChavesEmpresasExpDadosSOC empChave = repoConsultaWs.ListChaveEmpresasSoc.FirstOrDefault(x => x.Codigo == codigoEmpresa);

            if (empChave?.chaveAsoEsocial_7897 != null && empChave.chaveAsosPeriodo_6018 != null && empChave.chaveCadastroDinamico_7080 != null && empChave.chaveEmpFuncionarioSituacaoFol != null)
            {
                repoConsultaWs.RetornaFuncionariosSocWs("parametro={'empresa': " + codigoEmpresa + ",'codigo':'4237','chave':'" + empChave.chaveEmpFuncionarioSituacaoFol + "','tipoSaida':'json','ativo':'sim','inativo':'sim','afastado':'sim','pendente':'nao','ferias':'sim'}");

                repoConsultaWs.RetornarDadosDinamicosWs("parametro={'empresa': " + codigoEmpresa + ",'codigo':'7080','chave':'" + empChave.chaveCadastroDinamico_7080 + "','tipoSaida':'json','tipoPersonalizacao':'1','codigoTipoPersonalizacao':'1001'}");

                repoConsultaWs.RetornarAsosPeriodoEmpresaWs("parametro={'empresa': " + codigoEmpresa + ",'codigo':'6018','chave':'" + empChave.chaveAsosPeriodo_6018 + "','tipoSaida':'json','dataInicio':'" + DataInicio.ToString("d") + "','dataFim': '" + DataFim.Date.ToString("d") + "'}");

                repoConsultaWs.RetornarDadosAsoeSocialaWs("parametro={'empresa': " + codigoEmpresa + ",'codigo':'7897','chave':" + empChave.chaveAsoEsocial_7897 + ",'tipoSaida':'json', 'funcionarioInicio':'0','funcionarioFim':'999999999','pFuncionario':'0','funcionario':'0','dataInicio':'" +
                                           DataInicio.ToString("d") + "','dataFim':'" + DataFim.ToString("d") + "','pDataIncAso':'0','tpExame':'1,2,3,4,5,6'}");

                return true;
            }

            return false;
        }

        public static List<string> MontarArquivoRelatorioFomento()
        {
            int admissionalValido = 0,
                demissionalValido,
                periodicoValido = 0,
                mudancaFuncaoValido = 0,
                dadosSeguranca = 1,
                trabalhadoresPCMAT = 0,
                totalEmpresaAdmissionalValido,
                totalEmpresaPeriodicoValido = 0,
                totalEmpresaMudancaFuncaoValido = 0,
                totalEmpresaRetornoTrabalhoValido = 0,
                totalEmpresaDemissionalValido = 0,
                totalEmpresaretornoTrabalhoValido = 0,
                totalEmpresaDadosSeguranca = 0;

            List<string> linhas = new List<string>();
            ListLog.Add("CODIGO;NOME;RAZAOSOCIAL;UF");

            string cabecalhoTipo1 = "1" + EstadoDr + "" + DateTime.Now.ToString("yyyy-MM-dd") + "" + DataInicio.ToString("yyyy-MM-dd") + "" + DataInicio.ToString("yyyy-MM-dd");
            linhas.Add(cabecalhoTipo1);

            foreach (var itemEmp in repoConsultaWs.ListEmpresaSoc.Where(x => x.UF == EstadoDr))
            {
                string cabecalhoTipo2 = string.Empty;
                string numeroTap = string.Empty;
                string cnae = string.Empty;
                string ppra = string.Empty;
                string pcmso = string.Empty;
                string pcmsoExterno = string.Empty;
                string pcmat = string.Empty;
                string cei = string.Empty;
                string grauRisco = string.Empty;
                string espacoEmBranco = String.Empty;

                totalEmpresaAdmissionalValido = 0;
                totalEmpresaPeriodicoValido = 0;
                totalEmpresaMudancaFuncaoValido = 0;
                totalEmpresaRetornoTrabalhoValido = 0;
                totalEmpresaDemissionalValido = 0;
                totalEmpresaretornoTrabalhoValido = 0;

                if (CarregarDadosEmpresa(Convert.ToInt32(itemEmp.CODIGOEMPRESA)))
                {
                    if (repoConsultaWs.ListDadosDinamico != null && repoConsultaWs.ListDadosDinamico.Any())
                    {
                        numeroTap = repoConsultaWs.ListDadosDinamico.FirstOrDefault(d => d.CODIGOEMPRESA == itemEmp.CODIGOEMPRESA.ToString() && d.ITEM == "20")?.RESPOSTA ?? "";
                        cnae = repoConsultaWs.ListDadosDinamico.FirstOrDefault(d => d.CODIGOEMPRESA == itemEmp.CODIGOUNIDADE && d.ITEM == "32")?.RESPOSTA ?? "";
                        ppra = repoConsultaWs.ListDadosDinamico.FirstOrDefault(d => d.CODIGOEMPRESA == itemEmp.CODIGOUNIDADE && d.ITEM == "16" && (d.RESPOSTA == "1" || d.RESPOSTA == "2")) != null ? "1" : "0";
                        pcmso = repoConsultaWs.ListDadosDinamico.FirstOrDefault(d => d.CODIGOEMPRESA == itemEmp.CODIGOUNIDADE && d.ITEM == "16" && (d.RESPOSTA == "1" || d.RESPOSTA == "3" || d.RESPOSTA == "4")) != null ? "1" : "0";
                        pcmsoExterno = "0";
                        pcmat = repoConsultaWs.ListDadosDinamico.FirstOrDefault(d => d.CODIGOEMPRESA == itemEmp.CODIGOUNIDADE && d.ITEM == "16" && (d.RESPOSTA == "4" || d.RESPOSTA == "5")) != null ? "1" : "0";
                        cei = "000000000000";
                        grauRisco = repoConsultaWs.ListDadosDinamico.FirstOrDefault(d => d.CODIGOEMPRESA == itemEmp.CODIGOUNIDADE && d.ITEM == "32")?.RESPOSTA ?? "0";
                    }


                    DadosContatoEmpresasSOC contatoEmp = repoConsultaWs.ListContatosEmpresaSoc.FirstOrDefault(x => x.CODIGOEMPRESA == Convert.ToInt32(itemEmp.CODIGOEMPRESA));

                    if (contatoEmp != null)
                    {
                        cabecalhoTipo2 = "2" + itemEmp.NOMEEMPRESA.PadRight(150, ' ') + itemEmp.CNPJUNIDADE.Replace(".", "").Replace("/", "").Replace("-", "").PadLeft(15, '0') + numeroTap.PadLeft(10, '0') + contatoEmp.NOMECONTATO.PadRight(150, ' ') +
                                         contatoEmp.EMAIL1.PadRight(150, ' ') + " " + contatoEmp.TEL1.PadRight(10, ' ') + cnae.PadRight(9, ' ') + ppra + pcmso + pcmsoExterno + pcmat + cei + grauRisco.PadRight(2, '0');
                    }
                    else
                    {
                        cabecalhoTipo2 = "2" + itemEmp.NOMEEMPRESA.PadRight(150, ' ') + itemEmp.CNPJUNIDADE.Replace(".", "").Replace("/", "").Replace("-", "").PadLeft(15, '0') + numeroTap.PadLeft(10, '0') + espacoEmBranco.PadRight(150, ' ') +
                                         espacoEmBranco.PadRight(150, ' ') + " " + espacoEmBranco.PadRight(10, ' ') + cnae.PadRight(9, ' ') + ppra + pcmso + pcmsoExterno + pcmat + cei + grauRisco.PadRight(2, '0'); ;
                    }

                    if (string.IsNullOrEmpty(numeroTap))
                    {
                        ListLog.Add(itemEmp.CODIGOEMPRESA + ";" + itemEmp.NOMEEMPRESA + ";" + itemEmp.NOMEUNIDADE + ";" + itemEmp.UF);
                        System.Console.WriteLine("Empresa SEM TAP: =>" + itemEmp.NOMEEMPRESA);
                    }
                    else
                    {

                        System.Console.WriteLine("Empresa : =>" + itemEmp.NOMEEMPRESA + " func" + repoConsultaWs.ListFuncionarios.Count);

                        linhas.Add(cabecalhoTipo2);

                        int contadorFuncionario = 1;

                        int codigoEmpresa = Convert.ToInt32(itemEmp.CODIGOEMPRESA);

                        string examesDoFuncionário2035 = repoConsultaWs.ListChaveEmpresasSoc
                            .FirstOrDefault(x => x.Codigo == codigoEmpresa)?.ExameFuncionario_2035;

                        repoConsultaWs.RetornarDadosExameFuncionario(EstadoDr, itemEmp.EmpresaID );


                        foreach (var itemFunc in repoConsultaWs.ListFuncionarios)
                        {
                            //repoConsultaWs.RetornarDadosExameFuncionarioaWs("parametro={'empresa':'" + itemEmp.CODIGOEMPRESA +
                            //    "','codigo':'2035','chave':'" + examesDoFuncionário2035 + "','tipoSaida':'json','funcionario':'" + itemFunc.CODIGO + "'}");

                            

                            //try
                            //{
                            //    if (repoConsultaWs.ListExamesFuncionarios.Any())
                            //    {
                            //        foreach (var item in repoConsultaWs.ListExamesFuncionarios)
                            //        {
                            //            ExamesFuncionarioSOC ef = new ExamesFuncionarioSOC();
                            //            ef.DataInclusao = DateTime.Now;
                            //            ef.EmpresaSocID = itemEmp.EmpresaID;
                            //            ef.nomeEmpresa = itemEmp.NOMEEMPRESA;
                            //            ef.codigoExame = item.codigoExame;
                            //            ef.nomeExame = item.nomeExame;
                            //            ef.periodicidade = item.periodicidade;
                            //            ef.CodFuncionario = itemFunc.CODIGO.ToString();
                            //            if (!string.IsNullOrEmpty(item.dataResultadoExame))
                            //                ef.dataResultadoExame = Convert.ToDateTime(item.dataResultadoExame);
                            //            if (!string.IsNullOrEmpty(item.dataRefazerExame))
                            //                ef.dataRefazerExame = Convert.ToDateTime(item.dataRefazerExame);

                            //            db.ExamesFuncionarioSOC.Add(ef);
                            //        }


                            //        db.SaveChanges();


                            //    }
                            //}
                            //catch (Exception e)
                            //{
                            //    System.Console.WriteLine(e);
                            //    throw;
                            //}



                            totalEmpresaAdmissionalValido += admissionalValido =
                                repoConsultaWs.ListAsosPeriodoEmpresa.Count(x =>
                                    x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "1");
                            totalEmpresaPeriodicoValido += periodicoValido =
                                repoConsultaWs.ListAsosPeriodoEmpresa.Count(x =>
                                    x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "2");
                            totalEmpresaMudancaFuncaoValido += mudancaFuncaoValido =
                                repoConsultaWs.ListAsosPeriodoEmpresa.Count(x =>
                                    x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "3");
                            totalEmpresaRetornoTrabalhoValido += totalEmpresaretornoTrabalhoValido =
                                repoConsultaWs.ListAsosPeriodoEmpresa.Count(x =>
                                    x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "4");
                            totalEmpresaDemissionalValido += demissionalValido =
                                repoConsultaWs.ListAsosPeriodoEmpresa.Count(x =>
                                    x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "5");

                            string dataAdmissionalValido = string.Empty;
                            if (repoConsultaWs.ListAsosPeriodoEmpresa.Any(x =>
                                x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "1"))
                                dataAdmissionalValido = Convert
                                    .ToDateTime(repoConsultaWs.ListAsosPeriodoEmpresa.FirstOrDefault(x =>
                                        x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "1")?.DTASO)
                                    .ToString("yyyy-MM-dd");

                            string dataPeriodicoValido = string.Empty;
                            if (repoConsultaWs.ListAsosPeriodoEmpresa.Any(x =>
                                x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "2"))
                                dataPeriodicoValido = Convert
                                    .ToDateTime(repoConsultaWs.ListAsosPeriodoEmpresa.FirstOrDefault(x =>
                                        x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "2")?.DTASO)
                                    .ToString("yyyy-MM-dd");

                            string dataudancaFuncaoValido = string.Empty;
                            if (repoConsultaWs.ListAsosPeriodoEmpresa.Any(x =>
                                x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "3"))
                                dataudancaFuncaoValido = Convert
                                    .ToDateTime(repoConsultaWs.ListAsosPeriodoEmpresa.FirstOrDefault(x =>
                                        x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "3")?.DTASO)
                                    .ToString("yyyy-MM-dd");

                            string dataretornoTrabalhoValido = string.Empty;
                            if (repoConsultaWs.ListAsosPeriodoEmpresa.Any(x =>
                                x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "4"))
                                dataretornoTrabalhoValido = Convert
                                    .ToDateTime(repoConsultaWs.ListAsosPeriodoEmpresa.FirstOrDefault(x =>
                                        x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "4")?.DTASO)
                                    .ToString("yyyy-MM-dd");


                            string datademissionalValido = string.Empty;
                            if (repoConsultaWs.ListAsosPeriodoEmpresa.Any(x =>
                                x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "5"))
                                datademissionalValido = Convert
                                    .ToDateTime(repoConsultaWs.ListAsosPeriodoEmpresa.FirstOrDefault(x =>
                                        x.CODIGOFUNCIONARIO == itemFunc.CODIGO && x.TPASO == "5")?.DTASO)
                                    .ToString("yyyy-MM-dd");

                            DateTime dataSeguranca = new DateTime();
                            DateTime dataVigenciaPpra = new DateTime();
                            DateTime dataVigenciaPcmat = new DateTime();

                            if (repoConsultaWs.ListDadosDinamico.Any(x =>
                                x.CODIGOEMPRESA == itemEmp.CODIGOEMPRESA.ToString() && x.ITEM == "22"))
                            {
                                dataVigenciaPpra = Convert.ToDateTime(
                                    repoConsultaWs.ListDadosDinamico.FirstOrDefault(x =>
                                        x.CODIGOEMPRESA == itemEmp.CODIGOEMPRESA.ToString() && x.ITEM == "22").RESPOSTA ?? "");
                            }
                            if (repoConsultaWs.ListDadosDinamico.Any(x =>
                                x.CODIGOEMPRESA == itemEmp.CODIGOEMPRESA.ToString() && x.ITEM == "26"))
                            {
                                dataVigenciaPcmat = Convert.ToDateTime(
                                    repoConsultaWs.ListDadosDinamico.FirstOrDefault(x =>
                                        x.CODIGOEMPRESA == itemEmp.CODIGOEMPRESA.ToString() && x.ITEM == "26").RESPOSTA ?? "");
                            }

                            DateTime dataAdmissao = Convert.ToDateTime(itemFunc.DATA_ADMISSAO);

                            if (ppra == "1" || pcmat == "1")
                            {
                                dadosSeguranca = 1;
                                totalEmpresaDadosSeguranca++;

                                if (ppra == "1")
                                {
                                    dataSeguranca = (DateTime.Compare(dataAdmissao, dataVigenciaPpra) < 0) ? dataAdmissao : dataVigenciaPpra;
                                }
                                else if (pcmat == "1")
                                {
                                    dataSeguranca = (DateTime.Compare(dataAdmissao, dataVigenciaPcmat) < 0) ? dataAdmissao : dataVigenciaPcmat;
                                }

                            }
                            else
                            {
                                dadosSeguranca = 0;
                            }

                            string sexo = (itemFunc.SEXO == "1") ? "M" : "F";

                            string deficiente = (itemFunc.DEFICIENTE == "1") ? "02" : "00";

                            string periodicidade = repoConsultaWs.ListExamesFuncionarios.OrderBy(o => o.periodicidade).FirstOrDefault()?.periodicidade ?? "12";

                            string dataDemissao = (!string.IsNullOrEmpty(itemFunc.DATA_DEMISSAO)) ? Convert.ToDateTime(itemFunc.DATA_DEMISSAO).ToString("yyyy-MM-dd") : "";

                            string cabecalhoTipo3 = "3" + itemFunc.NOME.PadRight(150, ' ') +
                                                    Convert.ToDateTime(itemFunc.DATA_NASCIMENTO)
                                                        .ToString("yyyy-MM-dd") +
                                                    "" +
                                                    sexo +
                                                    itemFunc.PIS.Replace(".", "").Replace("-", "").PadLeft(11, '0') +
                                                    deficiente +
                                                    periodicidade +
                                                    admissionalValido +
                                                    periodicoValido +
                                                    mudancaFuncaoValido +
                                                    totalEmpresaretornoTrabalhoValido +
                                                    demissionalValido +
                                                    dadosSeguranca +
                                                    dataAdmissionalValido.PadRight(10, ' ') +
                                                    dataPeriodicoValido.PadRight(10, ' ') +
                                                    dataudancaFuncaoValido.PadRight(10, ' ') +
                                                    dataretornoTrabalhoValido.PadRight(10, ' ') +
                                                    datademissionalValido.PadRight(10, ' ') +
                                                    dataSeguranca.ToString("yyyy-MM-dd").PadRight(10, '0') +
                                                    itemFunc.CPF.PadLeft(11, '0') +
                                                    Convert.ToDateTime(itemFunc.DATA_ADMISSAO).ToString("yyyy-MM-dd") +
                                                    dataDemissao;
                            

                            linhas.Add(cabecalhoTipo3);
                        }

                        string cabecalhoTipo4 = "4" +
                                                repoConsultaWs.ListFuncionarios.Count.ToString().PadLeft(6, '0') +
                                                admissionalValido.ToString().PadLeft(4, '0') +
                                                periodicoValido.ToString().PadLeft(4, '0') +
                                                mudancaFuncaoValido.ToString().PadLeft(4, '0') +
                                                totalEmpresaretornoTrabalhoValido.ToString().PadLeft(4, '0') +
                                                totalEmpresaretornoTrabalhoValido.ToString().PadLeft(4, '0') +
                                                totalEmpresaDadosSeguranca.ToString().PadLeft(4, '0') +
                                                trabalhadoresPCMAT.ToString().PadLeft(4, '0');

                        linhas.Add(cabecalhoTipo4);

                        TotalDrAdmissionalValido += totalEmpresaAdmissionalValido;
                        TotalDrPeriodicoValido += totalEmpresaPeriodicoValido;
                        TotalDrMudancaFuncaoValido += totalEmpresaMudancaFuncaoValido;
                        TotalDrRetornoTrabalhoValido += totalEmpresaRetornoTrabalhoValido;
                        TotalDrDemissionalValido += totalEmpresaDemissionalValido;
                        TotalDrretornoTrabalhoValido += totalEmpresaretornoTrabalhoValido;
                        TotalDrTrabalhadoresValidos += repoConsultaWs.ListFuncionarios.Count;
                        TotalDrDadosSeguranca += totalEmpresaDadosSeguranca;
                    }
                }
            }

            string cabecalhoTipo5 = "5" + repoConsultaWs.ListEmpresaSoc.Count + TotalDrTrabalhadoresValidos.ToString().PadLeft(6, '0') + TotalDrAdmissionalValido.ToString().PadLeft(4, '0') +
                                        TotalDrPeriodicoValido.ToString().PadLeft(4, '0') + TotalDrMudancaFuncaoValido.ToString().PadLeft(4, '0') +
                                        TotalDrretornoTrabalhoValido.ToString().PadLeft(4, '0') + TotalDrDemissionalValido.ToString().PadLeft(4, '0') +
                                        dadosSeguranca.ToString().PadLeft(4, '0');

            int numeroLinhas = linhas.Count + 2;
            linhas.Add(cabecalhoTipo5);

            string cabecalhoTipo6 = "6" + numeroLinhas.ToString().PadLeft(6, '0');
            linhas.Add(cabecalhoTipo6);

            return linhas;
        }
        public static void GerarLog(List<string> linhasArquivo)
        {
            System.IO.File.WriteAllLines(@"D:\FomentoLog" + DataInicio.Year + "_" + EstadoDr + "_" + DateTime.Now.Ticks + "_.csv", linhasArquivo, Encoding.Default);
        }

        public static void GerarArquivoRelatorioCsv(List<string> linhasArquivo)
        {
            System.IO.File.WriteAllLines(@"D:\Fomento" + DataInicio.Year + "_" + EstadoDr + "_" + DateTime.Now.Ticks + "_.txt", linhasArquivo, Encoding.Default);
        }

    }
}
